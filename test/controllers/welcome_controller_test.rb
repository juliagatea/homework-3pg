require 'test_helper'
class WelcomeControllerTest < ActionController::TestCase
  def setup
    @input = "foo bar baz wibble fizzbuzz fizz buzz"
  end
  
  test "should get index" do
    get :index
    assert_response :success
  end
  
  test "post input text" do
    post :index, text_input: @input
    assert_match "7-4-5-21-37", response.body
  end
end