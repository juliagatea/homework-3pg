require 'test_helper'
class MethodsTest < ActionController::TestCase
  def setup
    @input = "foo bar baz wibble fizzbuzz fizz buzz"
    @input1 = "foo bar baz wibble fizzãbuzz fizz buzz"
    @input2 = "Foobarbazw Ibblefizzb Uzzfizzbuz Z" 
  end
  
  test "remove non english alphabet" do
    eabc = Useful.new.only_english_abc(@input1)
    assert_match "foo bar baz wibble fizzbuzz fizz buzz", eabc
  end
  
  test "count original words" do
    count = Useful.new.count_words(@input)
    assert_match "7", count.to_s
  end
  
  test "create new words" do
    new_words = Useful.new.create_new_words(@input)
    assert_match "foobarbazw ibblefizzb uzzfizzbuz z", new_words
  end
  
  test "count newly created words" do
    new_words = Useful.new.create_new_words(@input)
    count = Useful.new.count_words(new_words)
    assert_match "4", count.to_s
  end
  
  test "capitalize each word" do
    capitalize = Useful.new.capitalize_each_first_word(@input)
    assert_match "Foo Bar Baz Wibble Fizzbuzz Fizz Buzz", capitalize
  end
  
  test "capitalize each vowel" do
    capitalize = Useful.new.capitalize_vowel(@input2)
    assert_match "Foobarbazw IbblEfizzb UzzfIzzbUz Z", capitalize
  end
  
  test "count consonants" do
    count = Useful.new.count_consonants(@input)
    assert_match "21", count.to_s
  end
  
  test "count uppercase vowels" do
    count = Useful.new.count_upper_case_vowels(@input2)
    assert_match "2", count.to_s
  end
end