require 'test_helper'
class LogicTest < ActionController::TestCase
  def setup
    @input = "foo bar baz wibble fizzbuzz fizz buzz"
    @input1 = "The quick brown fox jumps over the lazy dog" 
    @input2 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit" 
  end
  
  test "result input" do
    result = Useful.new.result(@input)
    assert_match "7-4-5-21-37", result[1]
  end
  
  test "result input1" do
    result = Useful.new.result(@input1)
    assert_match "9-4-4-24-43", result[1]
  end
  
  test "result input2" do
    result = Useful.new.result(@input2)
    assert_match "8-5-7-28-55", result[1]
  end
end

