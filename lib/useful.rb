class Useful
  def only_english_abc(string)
    string.gsub(/[^0-9A-Za-z ]/, '')
  end
    
  def count_words(string)
    string.split.size
  end
    
  def create_new_words(string)
    string = string.delete(' ')
    array = string.split("")
    array.each_with_index { |val, index| array.insert(index, " ") if index % 11 == 0 }
    array.shift
    array.join("")    
  end
    
  def capitalize_each_first_word(string)
    string.split.map(&:capitalize).join(' ')
  end
    
  def capitalize_vowel(string)
    array = string.split("")
    vowels_array = []
    array.each_with_index { |val, index| vowels_array << [index, val] if val.match(/a|e|i|o|u|A|E|I|O|U/) }
    vowels_array.each_with_index do |x, y|
      if (y - 1) >= 0
        if (!vowels_array.include? [x[0]-1,array[x[0]-1]]) && (!vowels_array.include? [x[0]-2,array[x[0]-2]]) && vowels_array[y-1][1].upcase == vowels_array[y-1][1]
          array[x[0]] = array[x[0]].upcase
          vowels_array[y][1] = vowels_array[y][1].upcase
        end
      end
    end
    array.join("") 
  end
    
  def count_upper_case_vowels(string)
    string.tr('^AEIOU', '').length
  end
    
  def count_consonants(string)
    string.tr('aeiouAEIOU ', '').length
  end
    
  def result(string)
    oeabc = only_english_abc(string)
    cow = count_words(oeabc)
    new_words = create_new_words(oeabc)
    capitalize = capitalize_each_first_word(new_words)
    capitalize = capitalize_vowel(capitalize)
    cncw = count_words(capitalize)
    cucv = count_upper_case_vowels(capitalize)
    cc = count_consonants(capitalize)
    return [capitalize, cow.to_s + "-" + cncw.to_s + "-" + cucv.to_s + "-" + cc.to_s + "-" + string.length.to_s]
  end
end