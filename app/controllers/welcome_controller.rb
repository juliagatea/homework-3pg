class WelcomeController < ApplicationController
  def index
    if params[:text_input].present?
      input = params[:text_input]
      @result = Useful.new.result(input)
    end
  end
end